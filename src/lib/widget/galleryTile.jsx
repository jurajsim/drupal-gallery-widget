'use strict';
import React from 'react';

export default class GalleryTile extends React.Component{
    render() {
        var styles = {
            SubtitleStyle: {
                'padding-top': '20px'
            }
        }
        console.log(this.props.backgroundStyle);
        return (
            <div className="tile">
                <div style={this.props.backgroundStyle} className="tile-card"
                     onClick={this.props.onClick}>
                    <div className="tile-card-header">
                        <div className="tile-card-header-title"
                             style={this.props.tileTitleStyle}>{this.props.tileTitle}</div>
                        <div className="tile-card-header-subtitle"
                             style={styles.SubtitleStyle}>{this.props.tileSubtitle}</div>
                    </div>
                    <div className="tile-card-image-wrapper">
                        <img className="tile-image" src={this.props.imageSrc}/>
                    </div>
                </div>
            </div>
        )
    }
}