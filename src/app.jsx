'use strict';
import React from 'react';
import ReactDOM from 'react-dom';
import GalleryWidget from './lib/widget/galleryWidget.jsx';
import axios from 'axios';
import PlantSort from "./lib/util/plantSort";

require('./scss/main.scss');
require('./templates/index.html');

export default class Widget extends React.Component {

    constructor(props) {
        super(props);

        this.apiEndpoint = document.getElementById('widget').getAttribute('apiEndpoint');
        this.galleryId = document.getElementById('widget').getAttribute('galleryId');
        this.apiKey = document.getElementById('widget').getAttribute('apiKey');
        this.registryUrl = document.getElementById('widget').getAttribute('registryUrl');

        this.state = {}
    }

    componentWillUnmount() {
        if (this.serverRequest) {
            this.serverRequest.abort();
        }
    }

    loadGalleryData() {
        var self = this;
        this.serverRequest = axios.post(this.apiEndpoint + this.galleryId, {apiKey: this.apiKey})
            .then(function (result) {
                var sorter = new PlantSort(result.data.gallery_settings.plants);
                self.state.plants = sorter.sortPlants(result.data.plants);
                self.setState(self.state);
            })
    }

    componentDidMount() {
        this.checkWebp();
    }

    checkWebp() {
        var img = new Image();
        img.onload = () => {
            var webp = !!(img.height > 0 && img.width > 0);
            this.setState({webp: webp});
            this.loadGalleryData();
        };
        img.onerror = () => {
            this.state.webp = false
            this.setState(this.state);
            this.loadGalleryData();
        };
        img.src = 'data:image/webp;base64,UklGRiQAAABXRUJQVlA4IBgAAAAwAQCdASoBAAEAAwA0JaQAA3AA/vuUAAA=';
    }

    render() {
        if (this.state.plants && this.state.webp !== undefined) {
            return (
                <GalleryWidget webp={this.state.webp} plants={this.state.plants} registryUrl={this.registryUrl}></GalleryWidget>
            );
        } else {
            return <div>Loading...</div>
        }
    }
}

ReactDOM.render(<Widget/>, document.getElementById('widget'));