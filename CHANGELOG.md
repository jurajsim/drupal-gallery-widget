# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) 
and this project adheres to [Semantic Versioning](http://semver.org/).

## [1.0.1] 2016-01-14 11:53
### Changed
- Major refactoring, split to more react components
- Load widgets by public code instead of ID

## [1.0.0] 2015-12-15 11:53
### Added
- First release